<?php
$youtube_link = 'https://www.youtube.com/watch?v=RnBy-fITh2o';
$paypal_link = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VE649NNSUQSKJ';
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Natal Solidário Goiás</title>

    <meta name="description" content="O Natal Solidário é um projeto social organizado por pessoas comuns, que com muita responsabilidade, carinho e dedicação, buscam levar mais alegria para o fim de ano das crianças de comunidades pobres da grande Goiânia." />
    <meta name="keywords" content="natal, solidario, natal solidario, solidariedade, goias, goiania" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

 
    <link rel="stylesheet" href="css/normalize.css" media="all">
    <link rel="stylesheet" href="css/style.css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic|Raleway:400,200,300,500,700,600,800,900' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery-1.11.1.min.js"></script>
     <script src="js/device.min.js"></script> <!--OPTIONAL JQUERY PLUGIN-->
    <script src="js/jquery.mb.YTPlayer.js"></script>
    <script src="js/custom.js"></script>
    
    
 <meta name="robots" content="noindex,follow" />
</head>

<body>
    
    <section class="big-background">
        <a id="bgndVideo" class="player" data-property="{videoURL:'<?php echo $youtube_link; ?>',containment:'body',autoPlay:true, mute:false, startAt:0, opacity:1}"></a>
        
        <div class="pattern"></div> 
            <div class="big-background-container">
                    <h1 class="big-background-title">NATAL SOLIDÁRIO</h1>
                    <div class="divider"></div>
                    <h1 id="colorize">VOCÊ, POR MAIS SORRISOS</h1>
                    <a href="<?php echo $paypal_link; ?>" class="big-background-btn">FAZER DOAÇÃO ONLINE</a>
            </div>                                                    
    </section>
    

   
    <div class="wrapper">

    <section class="about-section">
     
                <div class="about-section-container">
                    <h2 class="about-section-title">O PROJETO SOCIAL</h2>
                    <p>O Natal Solidário é um projeto social organizado por pessoas comuns, que com muita responsabilidade, carinho e dedicação, buscam levar mais alegria para o fim de ano das crianças de comunidades pobres da grande Goiânia.</p>
                    <a href="https://www.youtube.com/watch?v=RnBy-fITh2o" target="_blank" class="about-section-btn">Conheça o Projeto</a>
                </div>

    </section>
            

     <section class="small-background-section">
     <div class="pattern"></div>
                    <div class="small-background-container">
                        <h2 class="small-background-title"><span>NÓS ACOMPANHE</span></h2>
                         <ul class="socials">
                        <li><a href="https://www.facebook.com/natalsolidariogo" target="_blank"><i class="fa fa-facebook-square fa-3x"></i></a></li>
                        <li><a href="https://www.instagram.com/natalsolidario" target="_blank"><i class="fa fa-instagram fa-3x"></i></a></li>

                        </ul>
                    </div>
    </section>


</div>
    
</body>
</html>